---
sidebar_position: 5
---

# Mystery Word

- When press the show question button in control screen, it will show clue and answer as per character per box.
- When press the hide question button in control screen, it will hide clue and answer as per character per box.
    ``` js 
    toggleQuestion() {
      clearInterval(this.interval); // stop timer if timer is running.
      this.audio.pause();     // pause clock ticking mp3
      this.count10sec.pause();  // pause 10secCountDown mp3
      this.count5sec.pause(); // pause 5secCountDown mp3

      // set showQuestion: !showQuestion
      // set startCount: false
      // set showAns: false
      // set resetCount: false
      // for animation
      // set clickExtraKey: false
      // set clickPoint: false
      this.store.dispatch(updateShowQuestion());
      // update click timer for animation
      this.store.dispatch(
        updateClickTimer({ clickTimer: this.control.clickTimer = false })
      );

      // update control extraWord if show question is false when round 5
      if (!this.control.showQuestion) {
        // set extraWord visibility is false
        _.map(this.control.extraWord, obj => {
          obj.visible = false;
        });
      }
      // send control object to main and onethird screens
      this.broadcastScreens();
    }
    ```

- When press the show answer button in control screen, it will show the answer words.
- When press the hide answer button in control screen, it will hide the answer words.
    ``` js
    toggleAnswer() {
      clearInterval(this.interval);   // stop timer if timer is running.
      this.audio.pause();         // pause clock ticking mp3
      this.count10sec.pause();    // pause 10secCountDown mp3
      this.count5sec.pause();     // pause 5secCountDown mp3

      // update extraWord visibility for round 5
      if (!this.control.showAns && this.currentRound.questionType == 5) {
        // set extraWord visibility is true and update and dispatch extraWord to reducer 
        _.map(this.control.extraWord, extraWord => {
          extraWord.visible = true;
        });
        this.store.dispatch(
          updateExraWord({ extraWord: this.control.extraWord })
        );
        this.store.dispatch(animatedExtraWord({ animationExtraWord: "" }));
      } else if (this.control.showAns && this.currentRound.questionType == 5) {
        // set extraWord visibility is false and update and dispatch extraWord to reducer 
        _.map(this.control.extraWord, extraWord => {
          extraWord.visible = false;
        });
        this.store.dispatch(
          updateExraWord({ extraWord: this.control.extraWord })
        );
      }

      // play audio if press Show Answer button
      if (!this.control.showAns) this.correct_answer_audio.play();

      // update control state
      this.control.startCount = false;
      this.control.showAns = !this.control.showAns;
      this.control.clickPoint = false;
      this.control.resetCount = false;
      // update click timer for animation
      this.store.dispatch(
        updateClickTimer({ clickTimer: this.control.clickTimer = false })
      );
      // send control object to main and onethird screens
      this.broadcastScreens();
    }
    ```

- When press the wrong answer button in control screen, wrong answer mp3 will play.
    ``` js
    wrongAnswer() {
      // play wrong answer mp3
      this.wrong_answer_audio.play();
    }
    ```

- When press the start timer button in control screen, it will start count.
- When press the stop timer button in control screen, it will stop count.
    ``` js
    startTimer(isStart) {
      //showQuestion automatically in round 4
      if (this.currentRound.hasCategory) {
        this.control.showQuestion = true;
        this.isStartRound4 = true;
      }

      // set startCount = startCount
      // set clickExtraKey = false
      // set clickPoint = false
      // set resetCount = false
      this.store.dispatch(updateStartCount({ control: this.control }));
      // update click timer for animation
      this.store.dispatch(
        updateClickTimer({ clickTimer: this.control.clickTimer = true })
      );
      // round 5 animation
      this.store.dispatch(animatedExtraWord({ animationExtraWord: "" }));
      // send control object to main and onethird screens
      this.broadcastScreens();

      // run timer
      if (this.control.startCount && isStart) {
        //only play the clock ticking sound timer is above 5 and 10
        if (this.currentRound.timeOut == 10) {
          if (this.timeOut > 5) this.audio.play();
        } else {
          if (this.timeOut > 10) this.audio.play();
        }

        this.interval = setInterval(() => {
          if (this.timeOut <= 0) {
            //disable start timer button
            this.disableStart = true;
            // stop timer if timer is less than and equal 0
            clearInterval(this.interval);

            // set startCount = startCount
            // set clickExtraKey = false
            // set clickPoint = false
            // set resetCount = false
            this.store.dispatch(updateStartCount({ control: this.control }));
            // send control object to main and onethird screens
            this.broadcastScreens();

            //show CorrectAnswer count for round 4 after timeout and stop running round 4
            if (this.currentRound.hasCategory) {
              //show corrected count
              this.correctAnswerCount = _.filter(
                this.questionArraysByCategory[this.currentCategory.id],
                question => question.actions == 1
              ).length;

              this.finishCategoryRound = true;
              this.store.dispatch(endCategoryRound());
              this.broadcastScreens();
              this.runCategoryRound = false;
            }
          } else {
            this.timeOut = this.timeOut - 1;  // dercrease timer -1
            if (this.currentRound.timeOut == 10) {
              if (this.timeOut < 6) {
                this.audio.pause(); // pause clock ticking mp3
                this.audio.currentTime = 0;   // reset audio
                this.count5sec.play();  // play 5secCountDown mp3
              }
            } else {
              if (this.timeOut < 13) {
                this.audio.pause();   // pause clock ticking mp3
                this.audio.currentTime = 0;   // reset audio
                this.count10sec.play();  // play 10secCountDown mp3
              }
            }
          }
        }, 1000);
      } else {
        // stop timer
        clearInterval(this.interval);
        this.audio.pause();
        this.count10sec.pause();
        this.count5sec.pause();
      }
    }
    ```

- When press the reset timer button in control screen, it will reset timer count to normal timer count (30sec).
    ``` js
    resetTimer() {
      this.timeOut = this.currentRound.timeOut;   // set timer to normal timeout
      this.disableStart = false;
      this.resetAudio();  // reset audio this related with timer 

      if (this.currentRound.hasCategory) {
        //reset for round4 section
        this.changeCategory(this.currentCategory);
      }
      this.store.dispatch(resetTimeOut());
      // send control object to main and onethird screens
      this.broadcastScreens();
    }
    ```

- When press the reset button in control screen, it will hide answer.
    ``` js
    resetExtraKey() {
      // set extraWord visibility is false
      _.map(this.control.extraWord, obj => {
        obj.visible = false;
      });
      this.updateKeyExtra();
    }
    updateKeyExtra(word?) {
      // update extraWord and animation for round 5
      this.store.dispatch(updateExraWord({ extraWord: this.control.extraWord }));
      this.store.dispatch(animatedExtraWord({ animationExtraWord: word }));
      // send control object to main and onethird screens
      this.broadcastScreens();
    }
    ```