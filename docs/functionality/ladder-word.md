---
sidebar_position: 6
---

# Ladder Word

- When press the start timer button in control screen, it will start count and show first question.
    ``` js 
    startTimer(isStart) {
      //showQuestion automatically in round 4
      if (this.currentRound.hasCategory) {
        this.control.showQuestion = true;
        this.isStartRound4 = true;
      }

      // set startCount = startCount
      // set clickExtraKey = false
      // set clickPoint = false
      // set resetCount = false
      this.store.dispatch(updateStartCount({ control: this.control }));
      // update click timer for animation
      this.store.dispatch(
        updateClickTimer({ clickTimer: this.control.clickTimer = true })
      );
      // round 5 animation
      this.store.dispatch(animatedExtraWord({ animationExtraWord: "" }));
      // send control object to main and onethird screens
      this.broadcastScreens();

      // run timer
      if (this.control.startCount && isStart) {
        //only play the clock ticking sound timer is above 5 and 10
        if(timer <= 5) this.count5sec.play();
        else{
          this.audio.play();
        }

        this.interval = setInterval(() => {
          if (this.timeOut <= 0) {
            //disable start timer button
            this.disableStart = true;
            // stop timer if timer is less than and equal 0
            clearInterval(this.interval);

            // set startCount = startCount
            // set clickExtraKey = false
            // set clickPoint = false
            // set resetCount = false
            this.store.dispatch(updateStartCount({ control: this.control }));
            // send control object to main and onethird screens
            this.broadcastScreens();

            //show CorrectAnswer count for round 4 after timeout and stop running round 4
            if (this.currentRound.hasCategory) {
              //show corrected count
              this.correctAnswerCount = _.filter(
                this.questionArraysByCategory[this.currentCategory.id],
                question => question.actions == 1
              ).length;

              this.finishCategoryRound = true;
              this.store.dispatch(endCategoryRound());
              this.broadcastScreens();
              this.runCategoryRound = false;
            }
          } else {
            this.timeOut = this.timeOut - 1;  // dercrease timer -1
            if(this.timeOut <= 5) {
              this.audio.pause();
              this.audio.currentTime = 0;
              this.count5sec.play();
            }
          }
        }, 1000);
      } else {
        // stop timer
        clearInterval(this.interval);
        this.audio.pause();
        this.count10sec.pause();
        this.count5sec.pause();
      }
    }
    ```
- When press the show answer button in control screen, it will show correct answer and move to next question.
  ``` js 
  correctAnswer() {
    console.log('current question: ', this.currentQuestion)
    // update store category round is running
    this.runCategoryRound = true;
    this.store.dispatch(runCategoryRound());
    this.store.dispatch(ShowAnsForCategoryRound());
    // check current question is answered or not
    if(this.currentQuestion['actions'] == undefined || this.currentQuestion['actions'] != 5){
      this.currentQuestion['actions'] = 5
      this.correctAnswerCount += 1; // increase correct answer count.
      this.correct_answer_audio.play();
    }
    // stop timer and stop running category round
    if(this.correctAnswerCount == this.questionArrForR4().length){
      this.startTimer(false);
      this.finishCategoryRound = true;
      this.store.dispatch(endCategoryRound());
      this.runCategoryRound = false;
    }
    this.broadcastScreens();
  }
  ```
- When press the next question button in control screen, it will move to next question and this button can use to skip question.
  ```js
  wrongOrskipQuestion(action) {
      // set runCategoryRound: true and dispatch to reducer
      this.runCategoryRound = true; 
      this.store.dispatch(runCategoryRound());
      //mark skip answer in separted arrays
      if (_.find(this.questionArraysByCategory[this.currentCategory.id], [
        "id",
        this.currentQuestion.id
      ]).actions == 0) {
        _.find(this.questionArraysByCategory[this.currentCategory.id], [
          "id",
          this.currentQuestion.id
        ]).actions = action;
      }
      //checkLastIndex or not
      let checkLastIndex = false;
      if (
        this.currentQuestion.id ==
        _.last(this.questionArraysByCategory[this.currentCategory.id]).id ||
        this.checkLastIndexExcludeSkip()
      )
        checkLastIndex = true;

      //check end of arrays by category
      if (this.loopQuestion || checkLastIndex) {
        //check exit skip question
        this.searchSkipQuestion(checkLastIndex);

        this.loopQuestion = true;
      } else {
        //show next question
        const nextQuest = this.nextQuestionByCategoryId();

        this.clickQuestion(nextQuest, this.currentQuestionIndex + 1);
      }
    }
  ```
- When press the reset timer button in control screen, it will reset timer count to normal timer count (50sec).
  ``` js
    resetTimer() {
      this.timeOut = this.currentRound.timeOut;   // set timer to normal timeout
      this.disableStart = false;
      this.resetAudio();  // reset audio this related with timer 

      if (this.currentRound.hasCategory) {
        //reset for round4 section
        this.changeCategory(this.currentCategory);
      }
      this.store.dispatch(resetTimeOut());
      // send control object to main and onethird screens
      this.broadcastScreens();
    }
    ```