---
sidebar_position: 2
---

# Json Structure

  ``` json title="src/assets/i18n/initData.json"
  {
      "episodes": [
      {
        "id": 1,
        "players": [
          {
            "id": 1,
            "name": "Player1",
            "point": 35,
            "pointFontSize": 300
          },
          {
            "id": 2,
            "name": "Player2",
            "point": 0,
            "pointFontSize": 300
          },
          {
            "id": 3,
            "name": "Player3",
            "point": 10,
            "pointFontSize": 300
          }
        ],
        "rounds": [
          {
            "id": 1,
            "name": "OPPOSITE WORD",
            "questionType": 1,
            "showfirstAnsChar": true,
            "hasCategory": false,
            "categories": [],
            "timeOut": 10,
            "point": 10,
            "secondTimeOut": 5,
            "secondPoint": 5,
            "questionArray": [
              {
                "id": 1,
                "clue": "MEND",
                "categoryId": 0,
                "hints": [],
                "ans": "BREAK",
                "isAnsCharacter": true,
                "clueFontSize": "45",
                "otClueFontSize": "45",
                "ansFontSize": "45",
                "otAnsFontSize": "45"
              },
              {
                "id": 2,
                "clue": "HIGH",
                "categoryId": 0,
                "hints": [],
                "ans": "LOW",
                "isAnsCharacter": true,
                "clueFontSize": "45",
                "otClueFontSize": "45",
                "ansFontSize": "45",
                "otAnsFontSize": "45"
              },
              {
                "id": 3,
                "clue": "STAY",
                "categoryId": 0,
                "hints": [],
                "ans": "LEAVE",
                "isAnsCharacter": true,
                "clueFontSize": "45",
                "otClueFontSize": "45",
                "ansFontSize": "45",
                "otAnsFontSize": "45"
              },
              {
                "id": 4,
                "clue": "CORRECT",
                "categoryId": 0,
                "hints": [],
                "ans": "WRONG",
                "isAnsCharacter": true,
                "clueFontSize": "45",
                "otClueFontSize": "45",
                "ansFontSize": "45",
                "otAnsFontSize": "45"
              },
              {
                "id": 5,
                "clue": "LEARN",
                "categoryId": 0,
                "hints": [],
                "ans": "TEACH",
                "isAnsCharacter": true,
                "clueFontSize": "45",
                "otClueFontSize": "45",
                "ansFontSize": "45",
                "otAnsFontSize": "45"
              },
              {
                "id": 6,
                "clue": "FRIEND",
                "categoryId": 0,
                "hints": [],
                "ans": "FOE",
                "isAnsCharacter": true,
                "clueFontSize": "45",
                "otClueFontSize": "45",
                "ansFontSize": "45",
                "otAnsFontSize": "45"
              },
              {
                "id": 7,
                "clue": "TOP",
                "categoryId": 0,
                "hints": [],
                "ans": "BOTTOM",
                "isAnsCharacter": true,
                "clueFontSize": "45",
                "otClueFontSize": "45",
                "ansFontSize": "45",
                "otAnsFontSize": "45"
              }
            ],
            "roundNameFontSize": "90",
            "clueLabelFontSize": "60",
            "roundName": "ROUND 1"
          },
          {
            "id": 2,
            "name": "CROSS WORD",
            "questionType": 2,
            "showfirstAnsChar": false,
            "hasCategory": false,
            "categories": [
              {
                "id": 0,
                "name": "WATER"
              },
              {
                "id": 1,
                "name": "test"
              }
            ],
            "timeOut": 15,
            "point": 15,
            "secondTimeOut": 5,
            "secondPoint": 10,
            "questionArray": [
              {
                "id": 1,
                "clue": "We swim laps at the ______",
                "categoryId": 0,
                "hints": [
                  {
                    "id": 1,
                    "value": "POOL",
                    "position": [
                      23,
                      24,
                      25,
                      26
                    ],
                    "isCharacter": false,
                    "hintFontSize": "45",
                    "otHintFontSize": "45"
                  }
                ],
                "ans": "26,",
                "isAnsCharacter": true,
                "clueFontSize": 50,
                "otClueFontSize": 50,
                "ansFontSize": 50,
                "otAnsFontSize": 50
              },
              {
                "id": 2,
                "clue": "Shallow vs ______",
                "categoryId": 0,
                "hints": [
                  {
                    "id": 1,
                    "value": "DEEP",
                    "position": [
                      106,
                      107,
                      108,
                      109
                    ],
                    "isCharacter": false,
                    "hintFontSize": "45",
                    "otHintFontSize": "45"
                  }
                ],
                "ans": "106,",
                "isAnsCharacter": true,
                "clueFontSize": 50,
                "otClueFontSize": 50,
                "ansFontSize": 50,
                "otAnsFontSize": 50
              },
              {
                "id": 3,
                "clue": "A small narrow river",
                "categoryId": 0,
                "hints": [
                  {
                    "id": 1,
                    "value": "STREAM",
                    "position": [
                      310,
                      410,
                      510,
                      610,
                      710,
                      810
                    ],
                    "isCharacter": false,
                    "hintFontSize": "45",
                    "otHintFontSize": "45"
                  }
                ],
                "ans": "810,",
                "isAnsCharacter": true,
                "clueFontSize": 50,
                "otClueFontSize": 50,
                "ansFontSize": 50,
                "otAnsFontSize": 50
              },
              {
                "id": 4,
                "clue": "A tank where fish lives",
                "categoryId": 0,
                "hints": [
                  {
                    "id": 1,
                    "value": "AQUARIUM",
                    "position": [
                      83,
                      84,
                      85,
                      86,
                      87,
                      88,
                      89,
                      810
                    ],
                    "isCharacter": false,
                    "hintFontSize": "45",
                    "otHintFontSize": "45"
                  }
                ],
                "ans": "88,",
                "isAnsCharacter": true,
                "clueFontSize": 50,
                "otClueFontSize": 50,
                "ansFontSize": 50,
                "otAnsFontSize": 50
              },
              {
                "id": 5,
                "clue": "A person who rescue swimmers",
                "categoryId": 0,
                "hints": [
                  {
                    "id": 1,
                    "value": "LIFEGUARD",
                    "position": [
                      26,
                      36,
                      46,
                      56,
                      66,
                      76,
                      86,
                      96,
                      106
                    ],
                    "isCharacter": false,
                    "hintFontSize": "45",
                    "otHintFontSize": "45"
                  }
                ],
                "ans": "86,",
                "isAnsCharacter": true,
                "clueFontSize": 50,
                "otClueFontSize": 50,
                "ansFontSize": 50,
                "otAnsFontSize": 50
              },
              {
                "id": 6,
                "clue": "Frozen water",
                "categoryId": 0,
                "hints": [
                  {
                    "id": 1,
                    "value": "ICE",
                    "position": [
                      36,
                      37,
                      38
                    ],
                    "isCharacter": false,
                    "hintFontSize": "45",
                    "otHintFontSize": "45"
                  }
                ],
                "ans": "36,",
                "isAnsCharacter": true,
                "clueFontSize": 50,
                "otClueFontSize": 50,
                "ansFontSize": 50,
                "otAnsFontSize": 50
              },
              {
                "id": 7,
                "clue": "Jumping into water head & arms first",
                "categoryId": 0,
                "hints": [
                  {
                    "id": 1,
                    "value": "DIVE",
                    "position": [
                      78,
                      88,
                      98,
                      108
                    ],
                    "isCharacter": false,
                    "hintFontSize": "45",
                    "otHintFontSize": "45"
                  }
                ],
                "ans": "108,",
                "isAnsCharacter": true,
                "clueFontSize": 50,
                "otClueFontSize": 50,
                "ansFontSize": 50,
                "otAnsFontSize": 50
              },
              {
                "id": 8,
                "clue": "Hold your ______ underwater",
                "categoryId": 0,
                "hints": [
                  {
                    "id": 1,
                    "value": "BREATH",
                    "position": [
                      53,
                      63,
                      73,
                      83,
                      93,
                      103
                    ],
                    "isCharacter": false,
                    "hintFontSize": "45",
                    "otHintFontSize": "45"
                  }
                ],
                "ans": "83,",
                "isAnsCharacter": true,
                "clueFontSize": 50,
                "otClueFontSize": 50,
                "ansFontSize": 50,
                "otAnsFontSize": 50
              },
              {
                "id": 9,
                "clue": "test",
                "categoryId": 1,
                "hints": [
                  {
                    "id": 1,
                    "value": "TEST",
                    "position": [
                      22,
                      23,
                      24,
                      25
                    ],
                    "isCharacter": false,
                    "hintFontSize": 50,
                    "otHintFontSize": 50
                  }
                ],
                "ans": "22,",
                "isAnsCharacter": true,
                "clueFontSize": 50,
                "otClueFontSize": 50,
                "ansFontSize": 50,
                "otAnsFontSize": 50
              }
            ],
            "roundNameFontSize": "90",
            "clueLabelFontSize": "60",
            "roundName": "ROUND 2"
          },
          {
            "id": 3,
            "name": "HIDDEN WORDS",
            "questionType": 3,
            "showfirstAnsChar": false,
            "hasCategory": false,
            "categories": [],
            "timeOut": 10,
            "point": 20,
            "secondTimeOut": 5,
            "secondPoint": 5,
            "questionArray": [
              {
                "id": 1,
                "clue": "DANCE",
                "categoryId": 0,
                "hints": [
                  {
                    "id": 1,
                    "value": "TANBALGOLET",
                    "position": 0,
                    "isCharacter": true,
                    "hintFontSize": 50,
                    "otHintFontSize": 50
                  }
                ],
                "ans": "TANGO BALLET",
                "isAnsCharacter": true,
                "clueFontSize": "60",
                "otClueFontSize": "60",
                "ansFontSize": "45",
                "otAnsFontSize": "45"
              },
              {
                "id": 2,
                "clue": "TELEPHONE",
                "categoryId": 0,
                "hints": [
                  {
                    "id": 1,
                    "value": "CALTALLK",
                    "position": 0,
                    "isCharacter": true,
                    "hintFontSize": 50,
                    "otHintFontSize": 50
                  }
                ],
                "ans": "CALL TALK",
                "isAnsCharacter": true,
                "clueFontSize": "60",
                "otClueFontSize": "60",
                "ansFontSize": "45",
                "otAnsFontSize": "45"
              },
              {
                "id": 3,
                "clue": "TIME",
                "categoryId": 0,
                "hints": [
                  {
                    "id": 1,
                    "value": "HOMINURUTE",
                    "position": 0,
                    "isCharacter": true,
                    "hintFontSize": 50,
                    "otHintFontSize": 50
                  }
                ],
                "ans": "HOUR MINUTE",
                "isAnsCharacter": true,
                "clueFontSize": "60",
                "otClueFontSize": "60",
                "ansFontSize": "45",
                "otAnsFontSize": "45"
              },
              {
                "id": 4,
                "clue": "SIZE",
                "categoryId": 0,
                "hints": [
                  {
                    "id": 1,
                    "value": "BISMGALL",
                    "position": 0,
                    "isCharacter": true,
                    "hintFontSize": 50,
                    "otHintFontSize": 50
                  }
                ],
                "ans": "BIG SMALL",
                "isAnsCharacter": true,
                "clueFontSize": "60",
                "otClueFontSize": "60",
                "ansFontSize": "45",
                "otAnsFontSize": "45"
              },
              {
                "id": 5,
                "clue": "TASTE",
                "categoryId": 0,
                "hints": [
                  {
                    "id": 1,
                    "value": "BITSOTERUR",
                    "position": 0,
                    "isCharacter": true,
                    "hintFontSize": 50,
                    "otHintFontSize": 50
                  }
                ],
                "ans": "BITTER SOUR",
                "isAnsCharacter": true,
                "clueFontSize": "60",
                "otClueFontSize": "60",
                "ansFontSize": "45",
                "otAnsFontSize": "45"
              },
              {
                "id": 6,
                "clue": "EMAIL",
                "categoryId": 0,
                "hints": [
                  {
                    "id": 1,
                    "value": "ADDINRESSBOX",
                    "position": 0,
                    "isCharacter": true,
                    "hintFontSize": 50,
                    "otHintFontSize": 50
                  }
                ],
                "ans": "ADDRESS INBOX",
                "isAnsCharacter": true,
                "clueFontSize": "60",
                "otClueFontSize": "60",
                "ansFontSize": "45",
                "otAnsFontSize": "45"
              },
              {
                "id": 7,
                "clue": "BICYCLE",
                "categoryId": 0,
                "hints": [
                  {
                    "id": 1,
                    "value": "WHEPEELDAL",
                    "position": 0,
                    "isCharacter": true,
                    "hintFontSize": 50,
                    "otHintFontSize": 50
                  }
                ],
                "ans": "WHEEL PEDAL",
                "isAnsCharacter": true,
                "clueFontSize": "60",
                "otClueFontSize": "60",
                "ansFontSize": "45",
                "otAnsFontSize": "45"
              }
            ],
            "roundNameFontSize": "90",
            "clueLabelFontSize": "60",
            "roundName": "ROUND 3"
          },
          {
            "id": 4,
            "name": "WORD LADDER",
            "questionType": 6,
            "showfirstAnsChar": false,
            "hasCategory": true,
            "categories": [
              {
                "id": 0,
                "name": "Player One",
                "digitCount": 5,
                "firstAnswer": "HELLO",
                "lastAnswer": "BABYS"
              },
              {
                "id": 1,
                "name": "Player Two",
                "digitCount": 4,
                "firstAnswer": "none",
                "lastAnswer": "tune"
              }
            ],
            "timeOut": 50,
            "point": 30,
            "questionArray": [
              {
                "id": 1,
                "clue": "question one",
                "categoryId": 0,
                "hints": [],
                "ans": "test",
                "isAnsCharacter": true,
                "clueFontSize": 50,
                "otClueFontSize": 50,
                "ansFontSize": 50,
                "otAnsFontSize": 50,
                "no": 1,
                "actions": 0
              },
              {
                "id": 2,
                "clue": "question two",
                "categoryId": 0,
                "hints": [],
                "ans": "twoww",
                "isAnsCharacter": true,
                "clueFontSize": 50,
                "otClueFontSize": 50,
                "ansFontSize": 50,
                "otAnsFontSize": 50,
                "no": 2,
                "actions": 0
              },
              {
                "id": 3,
                "clue": "question three",
                "categoryId": 0,
                "hints": [],
                "ans": "tesee",
                "isAnsCharacter": true,
                "clueFontSize": 50,
                "otClueFontSize": 50,
                "ansFontSize": 50,
                "otAnsFontSize": 50,
                "no": 3,
                "actions": 0
              },
              {
                "id": 4,
                "clue": "question one",
                "categoryId": 1,
                "hints": [],
                "ans": "onee",
                "isAnsCharacter": true,
                "clueFontSize": 50,
                "otClueFontSize": 50,
                "ansFontSize": 50,
                "otAnsFontSize": 50,
                "no": 1,
                "actions": 0
              },
              {
                "id": 5,
                "clue": "question two",
                "categoryId": 1,
                "hints": [],
                "ans": "toww",
                "isAnsCharacter": true,
                "clueFontSize": 50,
                "otClueFontSize": 50,
                "ansFontSize": 50,
                "otAnsFontSize": 50,
                "no": 2,
                "actions": 0
              },
              {
                "id": 6,
                "clue": "question three",
                "categoryId": 1,
                "hints": [],
                "ans": "ldfd",
                "isAnsCharacter": true,
                "clueFontSize": 50,
                "otClueFontSize": 50,
                "ansFontSize": 50,
                "otAnsFontSize": 50,
                "no": 3,
                "actions": 0
              }
            ],
            "roundNameFontSize": "90",
            "clueLabelFontSize": "60",
            "roundName": "ROUND 4"
          },
          {
            "id": 5,
            "name": "MYSTERY WORD",
            "questionType": 5,
            "showfirstAnsChar": false,
            "hasCategory": false,
            "categories": [],
            "timeOut": 30,
            "point": 30,
            "questionArray": [
              {
                "id": 1,
                "clue": "GREAT MENTAL ABILITY",
                "categoryId": 0,
                "hints": [],
                "ans": "INTELLIGENT",
                "isAnsCharacter": true,
                "clueFontSize": 45,
                "ansFontSize": 45,
                "otClueFontSize": 45,
                "otAnsFontSize": 45
              }
            ],
            "roundNameFontSize": "90",
            "clueLabelFontSize": "60",
            "roundName": "ROUND 5"
          }
        ]
      }
    ],
    "questionTypes": [
      {
        "id": 1,
        "name": "OPPOSITE WORD",
        "hintsCount": 0,
        "isHintChar": false,
        "isAnsChar": true,
        "showfirstAnsChar": true,
        "hasCategory": false
      },
      {
        "id": 2,
        "name": "CROSS WORD",
        "hintsCount": 1,
        "isHintChar": false,
        "isAnsChar": true,
        "showfirstAnsChar": false,
        "hasCategory": false
      },
      {
        "id": 3,
        "name": "HIDDEN WORDS",
        "hintsCount": 1,
        "isHintChar": true,
        "isAnsChar": true,
        "showfirstAnsChar": false,
        "hasCategory": false
      },
      {
        "id": 4,
        "name": "ODD WORD",
        "hintsCount": 3,
        "isHintChar": false,
        "isAnsChar": true,
        "showfirstAnsChar": false,
        "hasCategory": true
      },
      {
        "id": 5,
        "name": "MYSTERY WORD",
        "hintsCount": 0,
        "isHintChar": false,
        "isAnsChar": true,
        "showfirstAnsChar": false,
        "hasCategory": false
      },
      {
        "id": 6,
        "name": "WORD LADDER",
        "hintsCount": 0,
        "isHintChar": false,
        "isAnsChar": true,
        "showfirstAnsChar": true,
        "hasCategory": true
      }
    ],
    "fontSettings": {
      "main_roundName": "90",
      "main_clue": "60",
      "main_hint": 45,
      "main_answer": 45,
      "main_timeOut": 40,
      "oneThird_timeOut": 25,
      "oneThird_hint": 35,
      "oneThird_answer": 35,
      "player_point": 300
    }
  }
  ```