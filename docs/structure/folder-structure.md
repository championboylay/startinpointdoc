---
sidebar_position: 1
---

# Folder Structure

    ```sh
    wordwshiz
    ├── src
    │   ├── app
    │   │   ├── common
    │   │   │   ├── base64.ts
    │   │   │   ├── functions.ts
    │   │   │   └── images.ts
    │   │   ├── core
    │   │   │   ├── actions
    │   │   │   │   ├── control.actions.ts
    │   │   │   │   ├── episode.actions.ts
    │   │   │   │   └── wordWhiz.actions.ts
    │   │   │   ├── models
    │   │   │   │   ├── control.ts
    │   │   │   │   ├── episode.ts
    │   │   │   │   ├── extraWord.ts
    │   │   │   │   ├── fontSettings.ts
    │   │   │   │   ├── hint.ts
    │   │   │   │   ├── player.ts
    │   │   │   │   ├── question.ts
    │   │   │   │   ├── questionCategory.ts
    │   │   │   │   ├── questionTypes.ts
    │   │   │   │   ├── round.ts
    │   │   │   │   ├── roundTwoStatus.ts
    │   │   │   │   ├── screenTypeEnum.ts
    │   │   │   │   ├── timerEnum.ts
    │   │   │   │   └── wordWhiz.ts
    │   │   │   └── reducers
    │   │   │       ├── control.reducer.ts
    │   │   │       ├── episode.reducer.ts
    │   │   │       ├── index.reducer.ts
    │   │   │       └── wordWhiz.reducer.ts
    │   │   ├── shared
    │   │   │   ├── components
    │   │   │   │   └── page-not-found
    │   │   │   │       ├── page-not-found.component.html
    │   │   │   │       ├── page-not-found.component.scss
    │   │   │   │       └── page-not-found.component.ts
    │   │   │   ├── directives
    │   │   │   │   └── only-number.directive.ts
    │   │   │   └── shared.module.ts
    │   │   ├── views
    │   │   │   ├── control
    │   │   │   │   ├── control.component.html
    │   │   │   │   ├── control.component.scss
    │   │   │   │   ├── control.component.ts
    │   │   │   │   └── control.module.ts
    │   │   │   ├── main
    │   │   │   │   ├── main.component.html
    │   │   │   │   ├── main.component.scss
    │   │   │   │   ├── main.component.ts
    │   │   │   │   └── main.module.ts
    │   │   │   ├── oneThirdScreen
    │   │   │   │   ├── oneThirdScreen.component.html
    │   │   │   │   ├── oneThirdScreen.component.scss
    │   │   │   │   ├── oneThirdScreen.component.ts
    │   │   │   │   └── oneThirdScreen.module.ts
    │   │   │   ├── player
    │   │   │   │   ├── player.component.html
    │   │   │   │   ├── player.component.scss
    │   │   │   │   ├── player.component.ts
    │   │   │   │   └── player.module.ts
    │   │   │   ├── rounds
    │   │   │   │   ├── cross-word
    │   │   │   │   │   ├── cross-word.component.html
    │   │   │   │   │   ├── cross-word.component.scss
    │   │   │   │   │   └── cross-word.component.ts
    │   │   │   │   ├── hidden-word
    │   │   │   │   │   ├── hidden-word.component.html
    │   │   │   │   │   ├── hidden-word.component.scss
    │   │   │   │   │   └── hidden-word.component.ts
    │   │   │   │   ├── ladder-word
    │   │   │   │   │   ├── ladder-word.component.html
    │   │   │   │   │   ├── ladder-word.component.scss
    │   │   │   │   │   └── ladder-word.component.ts
    │   │   │   │   ├── mystery-word
    │   │   │   │   │   ├── mystery-word.component.html
    │   │   │   │   │   ├── mystery-word.component.scss
    │   │   │   │   │   └── mystery-word.component.ts
    │   │   │   │   ├── old-word
    │   │   │   │   │   ├── old-word.component.html
    │   │   │   │   │   ├── old-word.component.scss
    │   │   │   │   │   └── old-word.component.ts
    │   │   │   │   ├── opposite-word
    │   │   │   │   │   ├── opposite-word.component.html
    │   │   │   │   │   ├── opposite-word.component.scss
    │   │   │   │   │   └── opposite-word.component.ts
    │   │   │   │   └── rounds.module.ts
    │   │   │   ├── setup
    │   │   │   │   ├── setup.component.html
    │   │   │   │   ├── setup.component.scss
    │   │   │   │   ├── setup.component.ts
    │   │   │   │   └── setup.module.ts
    │   │   │   ├── home-routing.module.ts
    │   │   │   ├── home.component.html
    │   │   │   ├── home.component.scss
    │   │   │   ├── home.component.ts
    │   │   │   └──  home.module.ts
    │   │   ├── app-routing.module.ts
    │   │   ├── app.component.html
    │   │   ├── app.component.scss
    │   │   ├── app.component.ts
    │   │   └── app.module.ts
    │   ├── assets
    │   │   ├── audios
    │   │   │   ├── 3sec2beepCountDown.mp3
    │   │   │   ├── 5secCountDown-old.mp3
    │   │   │   ├── 5secCountDown.mp3
    │   │   │   ├── 10secCountDown-old.mp3
    │   │   │   ├── 10secCountDown.mp3
    │   │   │   ├── ClockTicking.mp3
    │   │   │   ├── Correct_answer.mp3
    │   │   │   └── Wrong_answer.mp3
    │   │   ├── fonts
    │   │   │   ├── DIN-Bold.otf
    │   │   │   └── fontSizeForWindow.json
    │   │   ├── i18n
    │   │   │   ├── addEpisodePlayers.json
    │   │   │   ├── addEpisoderRounds.json
    │   │   │   ├── en.json
    │   │   │   └── initData.json
    │   │   └── images
    │   │       ├── BLUE
    │   │       ├── A.png
    │   │       ├── ...
    │   │       └── Z.png
    │   │       ├── GREEN
    │   │       ├── A.png
    │   │       ├── ...
    │   │       └── Z.png
    │   │       ├── temp
    │   │       ├── grid-bg.png
    │   │       ├── ...
    │   │       └── setting.png
    │   ├── favicon.png
    │   ├── index.html
    │   ├── main.ts
    │   ├── styles.scss
    │   ├── polyfills.ts
    │   ├── test.ts
    ├── anglar.json
    ├── logo-angular.jpg
    ├── logo-electron.jpg
    ├── main.ts
    ├── package-lock.json
    ├── package.json
    ├── postinstall-web.js
    ├── postinstall.js
    ├── tsconfig-serve.json
    ├── splash.svg
    ├── tsconfig.json
    ├── tslint.json
    ```