---
sidebar_position: 5
---

# Hidden Word

In Round 3, there are 8 questions. Contestants have 15 seconds to find the correct word from the given word with blue box.
- When press the show question button in control screen, it will show clue, the answer of first character, green boxs and hint word.
- When press the hide question button in control screen, it will hide clue, the answer of first character, green boxs and hint word.
- When press the show answer button in control screen, it will show the answer words.
- When press the hide answer button in control screen, it will hide the answer words.
- When press the wrong answer button in control screen, wrong answer mp3 will play.
- When press the start timer button in control screen, it will start count.
- When press the stop timer button in control screen, it will stop count.
- When press the reset timer button in control screen, it will reset timer count to normal timer count (15sec).

## Json Structure

    ``` json
    {
        "id": 3,
        "name": "HIDDEN WORDS",
        "questionType": 3,
        "showfirstAnsChar": false,
        "hasCategory": false,
        "categories": [],
        "timeOut": 15,
        "point": 15,
        "questionArray": [
            {
                "id": 1,
                "clue": "LIVING THINGS",
                "categoryId": 0,
                "hints": [
                    {
                        "id": 1,
                        "value": "FLOFAURANA",
                        "position": 0,
                        "isCharacter": true,
                        "hintFontSize": 45,
                        "otHintFontSize": 45
                    }
                ],
                "ans": "FLORA FAUNA",
                "isAnsCharacter": true,
                "clueFontSize": 45,
                "ansFontSize": 45,
                "otClueFontSize": 45,
                "otAnsFontSize": 45
            }],
        "roundNameFontSize": "90",
        "clueLabelFontSize": "60"
    }
    ```
## Definition

- **id**, unique number of round.
- **name** is the round name(title).
- **questionType** is id type of question for current round. It can be seen in questionTypes json structure.
- **showfirstAnsChar** is boolean value and controls the first character of answer is shown or not.(Not use)
- **hasCategory**.(Not use)
- **categories** is json array and group questions with **categoryId**.**categories** is empty in *HIDDEN WORD*.(Not use)
- **timeOut** represents the how much time get to answer the question for contestants. In *HIDDEN WORD*, it is 15 ,  have 15 seconds to figure out the given word. It can be changed in admin.
- **point** is number, there is 15 points in *OPPOSITE WORDS*. If player can answer correct word, increase 15 points. It can be changed in admin.
- **questionArray** contains the json objects which are questions for current round.
    - **id** is the unique number of question.
    - **clue** is the string value. It will show as word in cube.
    - **categoryId** is based on the value of **categories**. Value will be 0, it is default value.If there is no **categories**, **categoryId** will not work.(Not use)
    - **hints** is json array and contains json object and found in *HIDDEN WORD* json structure.
        - **id**, unique number of hint.
        - **value** is string and will show in hint boxs base on **isCharacter** is true or false .
        - **position**.(Not use)
        - **isCharacter** is boolean value. If value is true, hint will be shown as per character per box. If value is false, hint will be shown in one box.
        - **hintFontSize** is the answer word of main screen font size. It can adjust this font size in control screen.
        - **otHintFontSize** is the answer word of onethird screen font size. It can adjust this font size in control screen.
    - **ans** is the string value and question of answer.
    - **isAnsCharacter** is boolean value. If value is true, answer will be shown as per character per box. If value is false, answer will be shown in one box.
    - **clueFontSize** is the clue word of main screen font size. It can adjust this font size in control screen.
    - **ansFontSize** is the answer word of main screen font size. It can adjust this font size in control screen.
    - **otClueFontSize** is the clue word of onethird screen font size. It can adjust this font size in control       screen.
    - **otAnsFontSize** is the answer word of onethird screen font size. It can adjust this font size in control screen.
- **roundNameFontSize** is the round name fount size. It can adjust this font size in control screen. 
- **clueLabelFontSize** is the clue word font size ('CLUE'). It can adjust this font size in control screen.