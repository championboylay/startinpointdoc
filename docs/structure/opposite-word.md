---
sidebar_position: 3
---

# Opposite Word

In Round 1, there are 8 questions. Contestants have 10 seconds to figure out the antonym of 
the given word.

- When press the show question button in control screen, it will show clue, the answer of first character and green boxs.
- When press the hide question button in control screen, it will hide clue and all answer boxs.
- When press the show answer button in control screen, it will show the remaining answer words.
- When press the hide answer button in control screen, it will hide the remaining answer words.
- When press the wrong answer button in control screen, wrong answer mp3 will play.
- When press the start timer button in control screen, it will start count.
- When press the stop timer button in control screen, it will stop count.
- When press the reset timer button in control screen, it will reset timer count to normal timer count(10sec).

## Json Structure

    ``` json
    {   
        "id": 1,
        "name": "OPPOSITE WORDS",
        "questionType": 1,
        "showfirstAnsChar": true,
        "hasCategory": false,
        "categories": [],
        "timeOut": 10,
        "point": 10,
        "questionArray": [
            {
                "id": 1,
                "clue": "SUNRISE",
                "categoryId": 0,
                "hints": [],
                "ans": "SUNSET",
                "isAnsCharacter": true,
                "clueFontSize": "45",
                "ansFontSize": "55",
                "otClueFontSize": 45,
                "otAnsFontSize": 45
            }
        ],
        "roundNameFontSize": "90",
        "clueLabelFontSize": "60"
    }
    ```
## Definition

- **id**, unique number of round.
- **name** is the round name(title).
- **questionType** is id type of question for current round. It can be seen in questionTypes json structure.
- **showfirstAnsChar** is boolean value and controls the first character of answer is shown or not.
- **hasCategory**, (Not use)
- **categories** is json array and group questions with **categoryId**.**categories** is empty in *OPPOSITE WORD*.(Not use)
- **timeOut** represents the how much time get to answer the question for contestants. In *OPPOSITE WORD*, it is 10, have 10 seconds to figure out the given word. It can be changed in admin.
- **point** is number, there is 10 points in *OPPOSITE WORDS*. If player can answer correct word, increase 10 points. It can be changed in admin.
- **questionArray** contains the json objects which are questions for current round.
    - **id** is the unique number of question.
    - **clue** is the string value. It will show as word in cube.
    - **categoryId** is based on the value of **categories**. Value will be 0, it is default value.If there is no **categories**, **categoryId** will not work. (Not use)
    - **hints** is json array and data is empty in *OPPOSITE WORD*.
    - **ans** is the string value and question of answer.
    - **isAnsCharacter** is boolean value. If value is true, answer will be shown as per character per box. If value is false, answer will be shown in one box.
    - **clueFontSize** is the clue word of main screen font size. It can adjust this font size in control screen.
    - **ansFontSize** is the answer word of main screen font size. It can adjust this font size in control screen.
    - **otClueFontSize** is the clue word of onethird screen font size. It can adjust this font size in control       screen.
    - **otAnsFontSize** is the answer word of onethird screen font size. It can adjust this font size in control screen.
- **roundNameFontSize** is the round name fount size. It can adjust this font size in control screen. 
- **clueLabelFontSize** is the clue word font size ('CLUE'). It can adjust this font size in control screen.