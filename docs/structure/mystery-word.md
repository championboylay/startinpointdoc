---
sidebar_position: 7
---

# Mystery Word

In Round 5, there are 1 questions. Contestants have 30 seconds to find out the given word.
- When press the show question button in control screen, it will show clue and answer as per character per box.
- When press the hide question button in control screen, it will hide clue and answer as per character per box.
- When press the show answer button in control screen, it will show the answer words.
- When press the hide answer button in control screen, it will hide the answer words.
- When press the wrong answer button in control screen, wrong answer mp3 will play.
- When press the start timer button in control screen, it will start count.
- When press the stop timer button in control screen, it will stop count.
- When press the reset timer button in control screen, it will reset timer count to normal timer count (30sec).
- When press the reset button in control screen, it will hide answer.

## Json Structure

    ``` json
    {
        "id": 5,
        "name": "MYSTERY WORD",
        "questionType": 5,
        "showfirstAnsChar": false,
        "hasCategory": false,
        "categories": [],
        "timeOut": 30,
        "point": 30,
        "questionArray": [
        {
            "id": 1,
            "clue": "GREAT MENTAL ABILITY",
            "categoryId": 0,
            "hints": [],
            "ans": "INTELLIGENT",
            "isAnsCharacter": true,
            "clueFontSize": 45,
            "ansFontSize": 45,
            "otClueFontSize": 45,
            "otAnsFontSize": 45
        }
        ],
        "roundNameFontSize": "90",
        "clueLabelFontSize": "60"
    }
    ```
## Definition

- **id**, unique number of round.
- **name** is the round name(title).
- **questionType** is id type of question for current round. It can be seen in questionTypes json structure.
- **showfirstAnsChar** is boolean value and controls the first character of answer is shown or not.(Not use)
- **hasCategory**.(Not use)
- **categories** is json array and group questions with **categoryId**.**categories** is empty in *MYSTERY WORD*.(Not use)
- **timeOut** represents the how much time get to answer the question for contestants. In *MYSTERY WORD*, it is 30 ,  have 30 seconds to figure out the given word. It can be changed in admin.
- **point** is number, there is 30 points in *OPPOSITE WORDS*. If player can answer correct word, increase 30 points. It can be changed in admin.
- **questionArray** contains the json objects which are questions for current round.
    - **id** is the unique number of question.
    - **clue** is the string value. It will show as word in cube.
    - **categoryId** is based on the value of **categories**. Value will be 0, it is default value.If there is no **categories**, **categoryId** will not work.(Not use)
    - **hints** is json array and data is empty in *MYSTERY WORD*.(Not use)
    - **ans** is the string value and question of answer.
    - **isAnsCharacter** is boolean value. If value is true, answer will be shown as per character per box. If value is false, answer will be shown in one box.
    - **clueFontSize** is the clue word of main screen font size. It can adjust this font size in control screen.
    - **ansFontSize** is the answer word of main screen font size. It can adjust this font size in control screen.
    - **otClueFontSize** is the clue word of onethird screen font size. It can adjust this font size in control       screen.
    - **otAnsFontSize** is the answer word of onethird screen font size. It can adjust this font size in control screen.
- **roundNameFontSize** is the round name fount size. It can adjust this font size in control screen. 
- **clueLabelFontSize** is the clue word font size ('CLUE'). It can adjust this font size in control screen.