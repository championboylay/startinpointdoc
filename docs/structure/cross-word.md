---
sidebar_position: 4
---

# Cross Word

In Round 2, there are 8 questions. Contestants are given the meaning of a particular phrase and 
have 10 seconds to figure out the phrase.Meaning of the phrase as well as the parts of the phrase given as clues.
Each question have category name and category id, it will show the answer of first character for hint.
- When press the show question button in control screen, it will show clue the answer of first character and show green boxs in grid.
- When press the hide question button in control screen, it will hide clue.
- When press the show answer button in control screen, it will show the remaining answer words.
- When press the wrong answer button in control screen, wrong answer mp3 will play.
- When press the start timer button in control screen, it will start count.
- When press the stop timer button in control screen, it will stop count.
- When press the reset timer button in control screen, it will reset timer count to normal timer count(10sec).

## Json Structure

    ``` json
    {   "id": 2,
        "name": "CROSS WORD",
        "questionType": 2,
        "showfirstAnsChar": false,
        "hasCategory": false,
        "categories": [
        {
            "id": 0,
            "name": "Fruit"
        },
        {
            "id": 1,
            "name": "Phone"
        }
        ],
        "timeOut": 10,
        "point": 10,
        "questionArray": [
        {
            "id": 1,
            "clue": "THIS COLOR IS RED AND CIRCLE",
            "categoryId": 0,
            "hints": [
            {
                "id": 1,
                "value": "APPLE",
                "position": [52,62,72,82,92],
                "isCharacter": false,
                "hintFontSize": "45",
                "otHintFontSize": "45"
            }
            ],
            "ans": "52,",
            "isAnsCharacter": true,
            "clueFontSize": 50,
            "otClueFontSize": 50,
            "ansFontSize": 50,
            "otAnsFontSize": 50
        }]
    }
    ```
## Definition

- **id**, unique number of round.
- **name** is the round name(title).
- **questionType** is id type of question for current round. It can be seen in questionTypes json structure.
- **showfirstAnsChar** is boolean value and controls the first character of answer is shown or not. (Not use)
- **hasCategory**. (Not use)
- **categories** is json array and group questions with **categoryId**.It can change group of questions by selecting category name in drop down.
- **timeOut** represents the how much time get to answer the question for contestants. In *CROSS WORD*, it is 10 ,  have 10 seconds to figure out the given word. It can be changed in admin.
- **point** is number, there is 10 points in *OPPOSITE WORDS*. If player can answer correct word, increase 10 points. It can be changed in admin.
- **questionArray** contains the json objects which are questions for current round.
    - **id** is the unique number of question.
    - **clue** is the string value. It will show as word in long blue box.
    - **categoryId** is based on the value of **categories**.There are categories in *CROSS WORD* and **categoryId** value will be **categories** json array of each object's id.
    - **hints** is json array and contains json object and found in *CROSS WORD* json structure.
        - **id**, unique number of hint.
        - **value** is string and will show in grid.
        - **position** note each **value** character with number in array.This number array's value represent the grid id to show in grid.
        - **isCharacter**.(Not use)
        - **hintFontSize** is the answer word of main screen font size. It can adjust this font size in control screen.
        - **otHintFontSize** is the answer word of onethird screen font size. It can adjust this font size in control screen.
    - **ans** is the string value and this string number will show word in grid.
    - **isAnsCharacter** is boolean value. If value is true, answer will be shown as per character per box. If value is false, answer will be shown in one box.(Not use)
    - **clueFontSize** is the clue word of main screen font size. It can adjust this font size in control screen.
    - **ansFontSize** is the answer word of main screen font size. It can adjust this font size in control screen.
    - **otClueFontSize** is the clue word of onethird screen font size. It can adjust this font size in control       screen.
    - **otAnsFontSize** is the answer word of onethird screen font size. It can adjust this font size in control screen.
- **roundNameFontSize** is the round name fount size. It can adjust this font size in control screen. 
- **clueLabelFontSize** is the clue word font size ('CLUE'). It can adjust this font size in control screen.