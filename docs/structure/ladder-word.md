---
sidebar_position: 8
---

# Ladder Word

In Round 6, there are 5 questions only per contestant.There are two categories, these are answer digit count is 4 letters or 6 letters. Contestants have 50 seconds to figure out the answer of the given word. Contestant can skip and come back to the skipped questions.
- When press the start timer button in control screen, it will start count and show first question.
- When press the show answer button in control screen, it will show correct answer and move to next question.
- When press the next question button in control screen, it will move to next question and this button can use to skip question.
- When press the reset timer button in control screen, it will reset timer count to normal timer count (50sec).

## Json Structure

    ```json
    {
    "id": 4,
    "name": "LADDER WORD",
    "questionType": 6,
    "showfirstAnsChar": false,
    "hasCategory": true,
    "categories": [
        {
        "id": 0,
        "name": "Player One",
        "digitCount": 6,
        "firstAnswer": "noneee",
        "lastAnswer": "tuneee"
        },
        {
        "id": 1,
        "name": "Player Two",
        "digitCount": 4,
        "firstAnswer": "none",
        "lastAnswer": "tune"
        }
    ],
    "timeOut": 50,
    "point": 30,
    "questionArray": [
        {
        "id": 1,
        "clue": "player one",
        "categoryId": 0,
        "hints": [],
        "ans": "TESTDD",
        "isAnsCharacter": true,
        "clueFontSize": 50,
        "otClueFontSize": 50,
        "ansFontSize": 50,
        "otAnsFontSize": 50,
        "no": 1,
        "actions": 3
        }
    ],
    "roundNameFontSize": "90",
    "clueLabelFontSize": "60",
    "roundName": "ROUND 4"
    }
    ```
## Definition

- **id**, unique number of round.
- **name** is the round name(title).
- **questionType** is id type of question for current round. It can be seen in questionTypes json structure.
- **showfirstAnsChar** is boolean value and controls the first character of answer is shown or not.(Not use)
- **hasCategory**  is boolean value that control the sub category of round. In *LADDER WORD*, there is category.
- **categories** is json array  and data are only exist based on hasCategory value. In *LADDER WORD*, hasCategory is true so that it is not empty.
- **digitCount** is number, this mean max answer letter count.
-**firstAnswer** is string, this answer always show at the top of ladder word.
-**lastAnswer** is string, this answer always show at the bottom of ladder word,
- **timeOut** represents the how much time get to answer the question for contestants. In *ODD WORD*, it is 60 ,  have 60 seconds to figure out the given word. It can be changed in admin.
- **point** is number, there is 10 points in *OPPOSITE WORDS*. If player can answer correct word, increase 10 points. It can be changed in admin.
- **questionArray** contains the json objects which are questions for current round.
    - **id** is the unique number of question.
    - **clue** is the string value. It will show as word in cube.
    - **categoryId** is based on the value of **categories**.There are categories in *ODD WORD* and **categoryId** value will be **categories** json array of each object's id.
    - **hints** is json array and data is empty in *MYSTERY WORD*.(Not use)
    - **ans** is the string value and question of answer.
    - **isAnsCharacter** is boolean value. If value is true, answer will be shown as per character per box. If value is false, answer will be shown in one box.
    - **clueFontSize** is the clue word of main screen font size. It can adjust this font size in control screen.
    - **ansFontSize** is the answer word of main screen font size. It can adjust this font size in control screen.
    - **otClueFontSize** is the clue word of onethird screen font size. It can adjust this font size in control       screen.
    - **otAnsFontSize** is the answer word of onethird screen font size. It can adjust this font size in control screen.
- **roundNameFontSize** is the round name fount size. It can adjust this font size in control screen. 
- **clueLabelFontSize** is the clue word font size ('CLUE'). It can adjust this font size in control screen.