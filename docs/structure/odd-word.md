---
sidebar_position: 6
---

# Odd Word

In Round 4, there are 8 questions. Contestants have 60 seconds to find the correct word from the given three words with blue box.
- When press the start timer button in control screen, it will start count and show three hints with blue boxs.
- When press the stop timer button in control screen, it will stop count.
- When press the next question button in control screen, it will show the next question in question list.
- When press the reset timer button in control screen, it will reset timer count to normal timer count (15sec).

## Json Structure

    ``` json
    {
        "id": 4,
        "name": "ODD WORD",
        "questionType": 4,
        "showfirstAnsChar": true,
        "hasCategory": true,
        "categories": [
        {
            "id": 0,
            "name": "SPACE"
        },
        {
            "id": 1,
            "name": "OCEAN"
        }
        ],
        "timeOut": 60,
        "point": 10,
        "questionArray": [
        {
            "id": 1,
            "clue": "question one",
            "categoryId": 0,
            "hints": [
            {
                "id": 1,
                "value": "MARS",
                "position": 0,
                "isCharacter": false,
                "hintFontSize": 45,
                "otHintFontSize": 45
            },
            {
                "id": 2,
                "value": "ARSM",
                "position": 0,
                "isCharacter": false,
                "hintFontSize": 45,
                "otHintFontSize": 45
            },
            {
                "id": 3,
                "value": "ARSM",
                "position": 0,
                "isCharacter": false,
                "hintFontSize": 45,
                "otHintFontSize": 45
            }
            ],
            "ans": "MARS",
            "isAnsCharacter": true,
            "ansFontSize": 45,
            "otAnsFontSize": 45,
            "clueFontSize": 46,
            "otClueFontSize": 46
        }],
            "roundNameFontSize": "90",
            "clueLabelFontSize": "60"
    }
    ```
## Definition

- **id**, unique number of round.
- **name** is the round name(title).
- **questionType** is id type of question for current round. It can be seen in questionTypes json structure.
- **showfirstAnsChar** is boolean value and controls the first character of answer is shown or not.(Not use)
- **hasCategory**  is boolean value that control the sub category of round. In *ODD WORD*, there is category.
- **categories** is json array  and data are only exist based on hasCategory value. In *ODD WORD*, hasCategory is true so that it is not empty.
- **timeOut** represents the how much time get to answer the question for contestants. In *ODD WORD*, it is 60 ,  have 60 seconds to figure out the given word. It can be changed in admin.
- **point** is number, there is 10 points in *OPPOSITE WORDS*. If player can answer correct word, increase 10 points. It can be changed in admin.
- **questionArray** contains the json objects which are questions for current round.
    - **id** is the unique number of question.
    - **clue** is the string value. It will show as word in cube.
    - **categoryId** is based on the value of **categories**.There are categories in *ODD WORD* and **categoryId** value will be **categories** json array of each object's id.
    - **hints** is json array and contains json object and found in *ODD WORD* json structure.
        - **id**, unique number of hint.
        - **value** is string and will show in hint boxs base on **isCharacter** is true or false .
        - **position**.(Not use)
        - **isCharacter** is boolean value. If value is true, hint will be shown as per character per box. If value is false, hint will be shown in one box.
        - **hintFontSize** is the answer word of main screen font size. It can adjust this font size in control screen.
        - **otHintFontSize** is the answer word of onethird screen font size. It can adjust this font size in control screen.
    - **ans** is the string value and question of answer.
    - **isAnsCharacter** is boolean value. If value is true, answer will be shown as per character per box. If value is false, answer will be shown in one box.
    - **clueFontSize** is the clue word of main screen font size. It can adjust this font size in control screen.
    - **ansFontSize** is the answer word of main screen font size. It can adjust this font size in control screen.
    - **otClueFontSize** is the clue word of onethird screen font size. It can adjust this font size in control       screen.
    - **otAnsFontSize** is the answer word of onethird screen font size. It can adjust this font size in control screen.
- **roundNameFontSize** is the round name fount size. It can adjust this font size in control screen. 
- **clueLabelFontSize** is the clue word font size ('CLUE'). It can adjust this font size in control screen.